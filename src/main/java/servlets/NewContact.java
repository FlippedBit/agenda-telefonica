/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package servlets;

import dao.AgendaDao;
import dao.ContactoDao;
import dao.NumeroDao;
import dao.TipoNumeroDao;
import dao.UsuarioContactoDao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelos.Agenda;
import modelos.Contacto;
import modelos.Numero;
import modelos.TipoDeNumero;

/**
 *
 * @author !FlippedBit
 *
 * El servlet 'NewContact' ingresa a la base de datos los datos ingresados por
 * el usuario en el formaulario de creación de nuevo contacto.
 */
public class NewContact extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession();
        Contacto nuevo;
        Numero num;
        nuevo = new Contacto(request.getParameter("nombreContacto"));
        List<Agenda> todos;
        List<TipoDeNumero> tipos;
        try {
            ContactoDao.add(nuevo);
            Contacto rec = ContactoDao.getLatest();
            num = new Numero(
                    Integer.parseInt(request.getParameter("tipoNum")),
                    rec.getIdContacto(),
                    Integer.parseInt(request.getParameter("NumeroContacto"))
            );
            NumeroDao.add(num);
            UsuarioContactoDao.addWithIds(
                    (int) sesion.getAttribute("userId"),
                    rec.getIdContacto()
            );
            todos = AgendaDao.getUserContacts((int) sesion.getAttribute("userId"));
            tipos = TipoNumeroDao.getTipos();
            sesion.setAttribute("tipos", tipos);
            sesion.setAttribute("lista", todos);
            response.sendRedirect("home.jsp");
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("error servlet: " + e.getMessage());
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
