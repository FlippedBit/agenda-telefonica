/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package servlets;

import dao.AgendaDao;
import dao.TipoNumeroDao;
import dao.UsuarioDao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelos.Agenda;
import modelos.TipoDeNumero;
import modelos.Usuario;

/**
 *
 * @author !FlippedBit
 * 
 * El servlet 'Autenticacion' se encarga de comparar las credenciales proporcionadas
 * en el formulario de inicio de sesión con las que se encuentran almacenadas en
 * la base de datos, probee una sesión HTTP y configura variables de sesión.
 */
public class Autenticacion extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            HttpSession sesion = request.getSession();
            String nombre, contra;
            Usuario usu;
            List<Agenda> todos;
            List<TipoDeNumero> tipos;
            nombre = request.getParameter("nombre");
            contra = request.getParameter("contra");
            
        try {
            usu = UsuarioDao.obtnUsuario(nombre);
            if(usu!=null){
                if(usu.getContrasena().equals(contra)){
                    todos = AgendaDao.getUserContacts(usu.getId());
                    tipos = TipoNumeroDao.getTipos();
                    sesion.setAttribute("tipos", tipos);
                    sesion.setAttribute("lista", todos);
                    sesion.setAttribute("userName", usu.getNombre());
                    sesion.setAttribute("userId", usu.getId());
                    sesion.setAttribute("IsAuthenticated", true);
                    response.sendRedirect("home.jsp");
                }else{
                    request.setAttribute("warn", "asd");
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                }
            }else{
                request.setAttribute("error", "asd");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }
        } catch (ClassNotFoundException |SQLException ex) {
            System.out.println("error servlet " + ex.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     * 
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
