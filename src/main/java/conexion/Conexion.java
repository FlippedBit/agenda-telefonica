/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author flippedbit
 *
 * La clase Conexion provee métodos para establecer una conexión con la base de
 * datos del proyecto.
 *
 */
public class Conexion {

    private static Connection con;
    private static String stringConexion;
    private static boolean conectado = false;

    /**
     *
     * @return @throws SQLException
     * @throws ClassNotFoundException
     *
     * Este método provee una conexión a la base de datos devolviendo un objeto
     * de java.sql.Connection
     */
    public static Connection obtnConexion() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        stringConexion = "jdbc:mysql://localhost:3306/agenda_telefonica?allowPublicKeyRetrieval=true&useSSL=false";
        con = DriverManager.getConnection(stringConexion, "root", "12345");
        conectado = true;
        return con;
    }

    /**
     *
     * @throws SQLException
     *
     * Este método permite cerrar una conexión abierta hacia la base de datos.
     * 
     */
    public static void close() throws SQLException {
        if (conectado) {
            try {
                con.close();
                conectado = false;
            } catch (SQLException e) {
                System.out.println("no se puede cerrar" + e.getMessage());
            }
        }
    }
}
