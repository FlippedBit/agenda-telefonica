/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dao;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelos.TipoDeNumero;

/**
 *
 * @author !FlippedBit
 * 
 * La clase TipoNumeroDao provee métodos para manipular la table 'tipo_de_numero'
 * de la base de datos.
 */
public class TipoNumeroDao {
    /**
     * 
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     * 
     * El método getTipos devuelve una lista de tipo 'TipoDeNumero' que contiene
     * los tipos de numero ingresados en la base de datos.
     */
    public static List<TipoDeNumero> getTipos() throws SQLException, ClassNotFoundException{
        
        Connection con;
        TipoDeNumero tdn;
        PreparedStatement ps;
        ResultSet rs;
        List<TipoDeNumero> tipos;
        tipos = new ArrayList<>();
        String sql="select * from tipo_de_numero";
        
        try{
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                tipos.add(
                    new TipoDeNumero(
                            rs.getInt("id_tipo"),
                            rs.getString("nombre_de_tipo")
                    )
                );
            }
            rs.close();
            ps.close();
            con.close();
            return tipos;
        }catch(SQLException e){
            System.out.println("error get, " + e.getMessage());
            tipos = null;
            return tipos;
        }
    }
    
}
