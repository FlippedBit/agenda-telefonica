/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dao;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelos.Agenda;

/**
 *
 * @author !FlippedBit
 *
 * La clase AgendaDao provee un método por el cual acceder a los contactos de un
 * usuario mediante una consulta multi-tabla a la base de datos;
 */
public class AgendaDao {

    /**
     *
     * @param id
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     *
     * El método getUserContacts recibe como parámetro una variable 'id' de tipo
     * int que representa la llave primaria del usuario del que se desea obtener
     * los contactos de la base de datos.
     */
    public static List<Agenda> getUserContacts(int id) throws SQLException, ClassNotFoundException {

        Connection con;
        Agenda age;
        PreparedStatement ps;
        ResultSet rs;
        List<Agenda> contactos;
        contactos = new ArrayList<>();
        String sql = "select usuario.nombre as usuario, "
                + "contacto.nombre as contacto, "
                + "numero.numero, "
                + "numero.id_numero, "
                + "usuario.id_usuario, "
                + "tipo_de_numero.nombre_de_tipo as 'tipo de numero', "
                + "usuario_contacto.id_rel "
                + "from usuario, contacto, numero, tipo_de_numero, usuario_contacto"
                + " where usuario.id_usuario=usuario_contacto.id_usuario "
                + "and contacto.id_contacto=usuario_contacto.id_contacto "
                + "and numero.id_tipo=tipo_de_numero.id_tipo "
                + "and contacto.id_contacto=numero.id_contacto "
                + "and usuario.id_usuario=? "
                + "order by contacto.id_contacto desc";

        try {
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                contactos.add(
                        age = new Agenda(
                                rs.getString("usuario"),
                                rs.getString("contacto"),
                                rs.getString("tipo de numero"),
                                rs.getInt("numero"),
                                rs.getInt("id_usuario"),
                                rs.getInt("id_numero"),
                                rs.getInt("id_rel")
                        )
                );
            }
            rs.close();
            ps.close();
            con.close();
            return contactos;
        } catch (SQLException e) {
            System.out.println("error get, " + e.getMessage());
            contactos = null;
            return contactos;
        }
    }

    public static Agenda getById(int idUsuario, int idRel) throws SQLException, ClassNotFoundException {
        Connection con;
        Agenda age;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select usuario.nombre as usuario, "
                + "contacto.nombre as contacto, "
                + "numero.numero, "
                + "numero.id_numero, "
                + "usuario.id_usuario, "
                + "tipo_de_numero.nombre_de_tipo as 'tipo de numero', "
                + "usuario_contacto.id_rel, "
                + "contacto.id_contacto "
                + "from usuario, contacto, numero, tipo_de_numero, usuario_contacto "
                + "where usuario.id_usuario=usuario_contacto.id_usuario "
                + "and contacto.id_contacto=usuario_contacto.id_contacto "
                + "and numero.id_tipo=tipo_de_numero.id_tipo "
                + "and contacto.id_contacto=numero.id_contacto "
                + "and usuario.id_usuario = ? "
                + "and usuario_contacto.id_rel = ? "
                + "order by contacto.id_contacto desc";

        try {
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, idUsuario);
            ps.setInt(2, idRel);
            rs = ps.executeQuery();
            rs.first();
            age = new Agenda(
                    rs.getString("usuario"),
                    rs.getString("contacto"),
                    rs.getString("tipo de numero"),
                    rs.getInt("numero"),
                    rs.getInt("id_usuario"),
                    rs.getInt("id_numero"),
                    rs.getInt("id_rel"),
                    rs.getInt("id_contacto")
            );
            rs.close();
            ps.close();
            con.close();
            return age;
        }catch(SQLException | ClassNotFoundException ex){
            System.out.println("error: " + ex.getMessage());
            age = null;
            return age;
        }
    }

}
