/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dao;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelos.Numero;

/**
 *
 * @author !FlippedBit
 * 
 * La clase NumeroDao provee métodos para manipular la table 'numero' de la base
 * de datos.
 */
public class NumeroDao {
    
    /**
     * 
     * @param num
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     * 
     * El método add recibe como parámetro un objeto Numero y lo ingresa
     * a la tabla 'numero' de la base de datos
     * 
     */
    public static boolean add(Numero num) throws SQLException, ClassNotFoundException{
        Connection con;
        PreparedStatement ps;
        String sql = "insert into numero(numero, id_tipo, id_contacto) values(?,?,?)";
        
        try{
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, num.getNumero());
            ps.setInt(2, num.getId_tipo());
            ps.setInt(3, num.getId_contacto());
            ps.executeUpdate();
            con.close();
            ps.close();
            return true;
        }catch(SQLException | ClassNotFoundException ex){
            System.out.println("Error " + ex.getMessage());
            return false; 
        }
    }
    
    
    /**
     * 
     * @return 
     * 
     * El método getLatest permite objener como un objeto Numero el 
     * registro más reciente de la tabla 'numero' de la base de datos
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public static Numero getLatest() throws SQLException, ClassNotFoundException{
        
        Connection con;
        Numero num;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select * from numero order by id_numero desc";
        
        try{
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.first();
            num = new Numero(
                    rs.getInt("id_numero"),
                    rs.getInt("id_tipo"),
                    rs.getInt("id_contacto"),
                    rs.getInt("numero")
            );
            rs.close();
            con.close();
            ps.close();
            return num;
        }catch(SQLException e){
            System.out.println("error get: " + e.getMessage());
            num = null;
            return num;
        }
        
    }
    
    /**
     * 
     * @param idNumero
     * @param newNumber
     * @return 
     * 
     * El método updateNumber permite actualizar un número de teléfono registrado
     * en la base de datos. Recibe como parámetros el id del registro que se desea
     * modificar y el nuevo número
     */
    public static boolean updateNumber(int idNumero, int newNumber) throws SQLException, ClassNotFoundException{
        
     Connection con;
     PreparedStatement ps;
     String sql = "update numero set numero=? where id_numero=?";
     
     try{
         con = Conexion.obtnConexion();
         ps = con.prepareStatement(sql);
         ps.setInt(1, newNumber);
         ps.setInt(2, idNumero);
         ps.executeUpdate();
         con.close();
         ps.close();
         return true;
     }catch(SQLException | ClassNotFoundException ex){
         System.out.println("error " + ex.getMessage());
         return false;
     }
        
    }
    
    /**
     * 
     * @param idNumero
     * @param idTipo
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     * 
     * El método updateType permite actualizar el tipo de un número registrado en 
     * la base de datos. Recibe como parámetros una variable int que representa
     * el id del registro que se desea modificar y otra que representa el id 
     * del tipo que se desea asignar.
     */
    public static boolean updateType(int idNumero, int idTipo) throws SQLException, ClassNotFoundException{
        
     Connection con;
     PreparedStatement ps;
     String sql = "update numero set id_tipo=? where id_numero=?";
     
     try{
         con = Conexion.obtnConexion();
         ps = con.prepareStatement(sql);
         ps.setInt(1, idTipo);
         ps.setInt(2, idNumero);
         ps.executeUpdate();
         con.close();
         ps.close();
         return true;
     }catch(SQLException | ClassNotFoundException ex){
         System.out.println("error " + ex.getMessage());
         return false;
     }
        
    }
    
}
