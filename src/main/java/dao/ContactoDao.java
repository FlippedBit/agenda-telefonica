/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dao;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelos.Contacto;

/**
 *
 * @author !FlippedBit
 *
 * La clase ContactoDao provee métodos para manipular la tabla 'contacto' de la
 * base de datos.
 */
public class ContactoDao {

    /**
     *
     * @param contact
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     *
     * El método add recibe como parámetro un objeto contacto y lo inserta a la
     * base de datos.
     */
    public static boolean add(Contacto contact) throws SQLException, ClassNotFoundException {
        Connection con;
        PreparedStatement ps;
        String sql = "insert into contacto(nombre) values(?)";

        try {
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, contact.getNombre());
            ps.executeUpdate();
            con.close();
            ps.close();
            return true;
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error " + ex.getMessage());
            return false;
        }
    }

    /**
     *
     * @param idContacto
     * @param name
     * @return
     *
     * El método updateName permite actualizar el nombre de un usuario
     * registrado en la base de datos. Recibe como parámetros una variable int
     * que representa la llave primaria del registro que se desea actualizar y
     * un String que conttiene el valor del nuevo nombre.
     */
    public static boolean updateName(int idContacto, String name) {
        Connection con;
        PreparedStatement ps;
        String sql = "update contacto set nombre=? where id_contacto=?";

        try {
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, idContacto);
            ps.executeUpdate();
            con.close();
            ps.close();
            return true;
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("error " + ex.getMessage());
            return false;
        }
    }

    /**
     *
     * @return @throws SQLException
     * @throws ClassNotFoundException
     *
     * El método getLatest permite obtener el registro más reciente de la tabla
     * cotacto de la base de datos.
     *
     */
    public static Contacto getLatest() throws SQLException, ClassNotFoundException {
        Connection con;
        Contacto contact;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select * from contacto order by id_contacto desc";

        try {
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rs.first();
            contact = new Contacto(
                    rs.getInt("id_contacto"),
                    rs.getString("nombre")
            );
            rs.close();
            con.close();
            ps.close();
            return contact;
        } catch (SQLException e) {
            System.out.println("error get: " + e.getMessage());
            contact = null;
            return contact;
        }
    }
}
