/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dao;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelos.Usuario;

/**
 *
 * @author !FlippedBit
 *
 * La clase UsuarioDao contiene los métodos que aplican a la clase Usuario para
 * la interacción con la tabla 'usuario' de la base de datos.
 *
 */
public class UsuarioDao {

    /**
     *
     * @param userName
     * @return
     *
     * Este método devuelve un objeto de tipo Usuario comparando el parámetro
     * recibido con las entradas de la base de datos.
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     *
     */
    public static Usuario obtnUsuario(String userName) throws ClassNotFoundException, SQLException{
        Connection con;
        Usuario usu;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select * from usuario "
                + "where nombre = ?";

        try {
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, userName);
            rs = ps.executeQuery();

            rs.first();
            usu = new Usuario(
                    rs.getInt("id_usuario"),
                    rs.getString("nombre"),
                    rs.getString("contrasena")
            );
            rs.close();
            ps.close();
            con.close();
            return usu;
        } catch (SQLException e) {
            System.out.println("error obtn, " + e.getMessage());
            usu = null;
            return usu;
        }
    }
    
    /**
     * 
     * @param usu
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     * 
     * El método regUsuario recibe como parámetro un objeto de tipo Usuario el cual
     * inserta a la tabla usuario de la base de datos
     */
    public static boolean regUsuario(Usuario usu) throws SQLException, ClassNotFoundException{
        Connection con;
        PreparedStatement ps;
        String sql = "insert into usuario(nombre, contrasena) values (?,?)";
        
        try{
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, usu.getNombre());
            ps.setString(2, usu.getContrasena());
            ps.executeUpdate();
            con.close();
            ps.close();
            return true;
        }catch(SQLException | ClassNotFoundException ex){
            System.out.println("Error: " + ex.getMessage());
            return false;
        }
    }

}
