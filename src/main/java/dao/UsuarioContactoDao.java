/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package dao;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author !FlippedBit
 * 
 * La clase UsuarioContactoDao provee métodos para la manipulación de la tabla
 * 'usuario_contacto' de la base de datos.
 */
public class UsuarioContactoDao {
    
    /**
     * 
     * @param usuario
     * @param contacto
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     * 
     * El método addWithIds permite agregar un registro a la table usuario_contactos
     * de la base de datos para relacionar un contacto a un usuario a través de 
     * los parámetros de tipo int 'usuario' y 'contacto' que representan la llave
     * primaria del usuario activo y del contacto que se desea relacionar.
     */
    public static boolean addWithIds(int usuario, int contacto) throws SQLException, ClassNotFoundException{
        Connection con;
        PreparedStatement ps;
        String sql = "insert into usuario_contacto(id_usuario, id_contacto) values(?,?)";
        
        try{
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, usuario);
            ps.setInt(2, contacto);
            ps.executeUpdate();
            ps.close();
            return true;
        }catch(SQLException | ClassNotFoundException ex){
            System.out.println("Error " + ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     * @param idRel
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException 
     * 
     * el método endRelation elimina la relación entre un usuario y uno de sus
     * contactos. Recible como parámetro una variable int que representa el id
     * del registro de la relación en la base de datos
     */
    public static boolean endRelation(int idRel) throws SQLException, ClassNotFoundException{
        Connection con;
        PreparedStatement ps;
        String sql = "delete from usuario_contacto where id_rel=?";
        
        try{
            con = Conexion.obtnConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, idRel);
            ps.executeUpdate();
            ps.close();
            return true;
        }catch(SQLException | ClassNotFoundException ex){
            System.out.println("Error: " + ex.getMessage());
            return false;
        }
    }
    
}
