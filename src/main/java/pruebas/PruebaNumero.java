/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package pruebas;

import dao.NumeroDao;
import java.sql.SQLException;
import modelos.Numero;

/**
 *
 * @author !FlippedBit
 */
public class PruebaNumero {
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        
//        int numero = 45678932;
//        int contacto = 3;
//        int tipo = 1;        
//        Numero num = new Numero(tipo, contacto, numero);
//        
//        if(NumeroDao.add(num)){
//            System.out.println("si ingreso");
//        }else{
//            System.out.println("no ingreso");
//        }
        
//        Numero num = NumeroDao.getLatest();
//        
//        System.out.println(num.getId_numero());

        int id = 1;
        int tipo = 2;
        
        if(NumeroDao.updateType(id, tipo)){
            System.out.println("si se pudo");
        }else{
            System.out.println("no se pudo");
        }

    }
    
}
