/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package pruebas;

import dao.UsuarioContactoDao;
import java.sql.SQLException;
import modelos.UsuarioContacto;

/**
 *
 * @author !FlippedBit
 */
public class PruebaUsuarioContacto {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

//        int usuario = 1;
//        int contacto = 3;
//        
//        if(UsuarioContactoDao.addWithIds(usuario, contacto)){
//            System.out.println("si ingresa");
//        }else{
//            System.out.println("no ingresa");
//        }
        int idRel = 12;
        
        if(UsuarioContactoDao.endRelation(idRel)){
            System.out.println("si elimina");
        }else{
            System.out.println("no elimina");
        }

    }
}
