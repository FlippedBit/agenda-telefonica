/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package pruebas;

import dao.AgendaDao;
import java.sql.SQLException;
import java.util.List;
import modelos.Agenda;

/**
 *
 * @author !FlippedBit
 */
public class PruebaAgenda {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
//        int id = 1;
//        List<Agenda> todos;
//        todos = AgendaDao.getUserContacts(id);
//        if (todos != null) {
//            for (Agenda uno : todos) {
//                System.out.println(uno.getUsuario() + " " + uno.getContacto() + " " + uno.getNumero() + " " + uno.getTipoDeNumero() + " " + uno.getIdUsuario() + " " + uno.getIdNumero() + " " + uno.getIdRel());
//            }
//        } else {
//            System.out.println("no sale! ;-;");
//        }

    int idUsuario = 1;
    int idRel = 6;
    
    Agenda age = AgendaDao.getById(idUsuario, idRel);
    
        System.out.println(age.getUsuario() + " " + age.getContacto() + " " + age.getNumero() + " " + age.getTipoDeNumero() + " " + age.getIdUsuario() + " " + age.getIdNumero() + " " + age.getIdRel() + " "  + age.getIdContacto());
    }

}
