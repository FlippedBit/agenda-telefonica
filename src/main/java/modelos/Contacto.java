/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modelos;

/**
 *
 * @author flippedbit
 *
 * Esta clase es un modelo que representa a la table 'contacto' de la base de
 * datos.
 */
public class Contacto {

    private int idContacto;
    private String nombre;

    public Contacto(int idContacto, String nombre) {
        this.idContacto = idContacto;
        this.nombre = nombre;
    }

    public Contacto(String nombre) {
        this.nombre = nombre;
    }

    public Contacto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdContacto() {
        return idContacto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

}
