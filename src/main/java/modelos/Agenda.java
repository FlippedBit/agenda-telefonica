/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modelos;

/**
 *
 * @author !FlippedBit
 *
 * La clase 'Agenda' es un modelo que representa las relaciones
 * 'Usuario->Contactos' y 'Contactos->Numero' de la base de datos.
 *
 */
public class Agenda {

    private String usuario;
    private String contacto;
    private String tipoDeNumero;
    private int numero;
    private int idUsuario;
    private int idNumero;
    private int idRel;
    private int idContacto;

    public Agenda(String usuario, String contacto, String tipoDeNumero, int numero, int idUsuario, int idNumero, int idRel) {
        this.usuario = usuario;
        this.contacto = contacto;
        this.tipoDeNumero = tipoDeNumero;
        this.numero = numero;
        this.idUsuario = idUsuario;
        this.idNumero = idNumero;
        this.idRel = idRel;
    }
    
    public Agenda(String usuario, String contacto, String tipoDeNumero, int numero, int idUsuario, int idNumero, int idRel, int idContacto) {
        this.usuario = usuario;
        this.contacto = contacto;
        this.tipoDeNumero = tipoDeNumero;
        this.numero = numero;
        this.idUsuario = idUsuario;
        this.idNumero = idNumero;
        this.idRel = idRel;
        this.idContacto = idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdRel(int idRel) {
        this.idRel = idRel;
    }

    public int getIdRel() {
        return idRel;
    }

    public void setTipoDeNumero(String tipoDeNumero) {
        this.tipoDeNumero = tipoDeNumero;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setIdNumero(int idNumero) {
        this.idNumero = idNumero;
    }

    public int getIdNumero() {
        return idNumero;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTipo_de_numero(String tipo_de_numero) {
        this.tipoDeNumero = tipo_de_numero;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContacto() {
        return contacto;
    }

    public int getNumero() {
        return numero;
    }

    public String getTipoDeNumero() {
        return tipoDeNumero;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

}
