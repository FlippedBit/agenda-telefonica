/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modelos;

/**
 *
 * @author flippedbit
 *
 * La clase Numero es un modelo que representa la table 'numero' de la base de
 * datos
 */
public class Numero {

    private int id_numero;
    private int id_tipo;
    private int id_contacto;
    private int numero;

    public Numero(int id_numero, int id_tipo, int id_contacto, int numero) {
        this.id_numero = id_numero;
        this.id_tipo = id_tipo;
        this.id_contacto = id_contacto;
        this.numero = numero;
    }

    public Numero(int id_tipo, int id_contacto, int numero) {
        this.id_tipo = id_tipo;
        this.id_contacto = id_contacto;
        this.numero = numero;
    }

    public int getId_numero() {
        return id_numero;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public int getNumero() {
        return numero;
    }

    public void setId_numero(int id_numero) {
        this.id_numero = id_numero;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

}
