/*
 * Copyright (C) 2019 !FlippedBit
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modelos;

/**
 *
 * @author !FlippedBit
 *
 * La clase UsuarioContacto es un modelo que representa a la tabla
 * usuario_contacto de la base de datos, la cual relaciona los usuarios de la
 * aplicación con los contactos registrados
 *
 */
public class UsuarioContacto {

    private int idUsuario;
    private int idContacto;
    private int idRel;

    public UsuarioContacto(int idUsuario, int idContacto) {
        this.idUsuario = idUsuario;
        this.idContacto = idContacto;
    }

    public int getIdRel() {
        return idRel;
    }

    public int getId_usuario() {
        return idUsuario;
    }

    public int getId_contacto() {
        return idContacto;
    }

    public void setIdRel(int idRel) {
        this.idRel = idRel;
    }        

    public void setId_usuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setId_contacto(int idContacto) {
        this.idContacto = idContacto;
    }

}
