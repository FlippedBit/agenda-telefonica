<%--
    Document   : update
    Created on : Mar 24, 2019, 8:06:18 PM
    Author     : !FlippedBit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar Contacto</title>
        <link rel="stylesheet" href="css/bulma.css">
        <link rel="stylesheet" href="css/all.css">
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <style media="screen">
        .is-dark{
            background-image:url("images/edit.png");
            background-blend-mode:multiply;
            background-size:contain;
        }
    </style>
    <body>
        <section class="hero is-dark">
            <div class="hero-body">
                <div class="container">
                    <h1 class="title is-1">
                        <b class="has-text-warning">Actualiza</b> los datos de <b class="has-text-primary"><c:out value="${contactName}"/></b>
                    </h1>
                    <h2 class="subtitle">
                        Ingresa la nueva información del contacto
                    </h2>
                </div>
                <div class="level">
                    <div class="level-left">
                    </div>
                    <div class="level-right">
                        <div class="level-item buttons" style="margin-right:115px;">
                            <p class="control">
                            <form action="home.jsp" method="post">
                                <button class="button is-warning is-rounded" name="button">
                                    <p>Volver</p>
                                    <span class="icon">
                                        <i class="fas fa-chevron-left"></i>
                                    </span>
                                </button>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="columns">
            <div class="column">
            </div>
            <section class="section column is-three-fifths">
                <div class="has-text-center box">
                    <h1 class="title">Nuevos datos de Contacto</h1>
                    <form action="UpdateContact" method="post">
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Nombre</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input name="idContact" type="hidden" value="<c:out value="${idContacto}"/>">
                                        <input 
                                            name="nombreContacto" 
                                            class="input" 
                                            type="text" 
                                            placeholder="Nombre de Contacto" 
                                            value="<c:out value="${contactName}"/>">
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Número</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <input type="hidden" name="numberId" value="<c:out value="${numberId}"/>">
                                    <p class="control has-icons-left">
                                        <input
                                            name="NumeroContacto"
                                            class="input"
                                            type="text"
                                            placeholder="Número de Contacto"
                                            pattern="^[1-9]+[0-9]*$"
                                            title="Este campo solo puede contener números"
                                            value="<c:out value="${contactNumber}"/>">
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-phone"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Tipo de Telefono</label>
                            </div>
                            <div class="field-body">
                                <div class="field is-narrow">
                                    <p class="control has-icons-left">
                                        <span class="select">
                                            <select name="tipoNum">
                                                <option selected>-----------</option>
                                                <c:forEach items="${tipos}" var="uno">
                                                    <option value="${uno.idTipo}">${uno.nombreTipo}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-list-ul"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label">
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-success">
                                            <span>
                                                <i class="fas fa-check"></i>
                                            </span>
                                            <p>&nbsp; Actualizar</p>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
            <div class="column">
            </div>
        </div>
    </body>
</html>
