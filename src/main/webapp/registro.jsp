<%--
    Document   : registro
    Created on : Mar 18, 2019, 9:00:06 PM
    Author     : !FlippedBit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Regístrate</title>
        <link rel="stylesheet" href="css/bulma.css">
        <link rel="stylesheet" href="css/all.css">
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <style media="screen">
        .is-primary{
            background-image: url("images/new.jpg");
            background-blend-mode: multiply
        }
    </style>
    <body>
        <section class="hero is-primary is-fullheight">
            <div class="hero-head">
                <div class="container">
                    <h1 class="title is-1">Agenda Telefónica</h1>
                </div>
            </div>
            <div class="hero-body center">
                <div class="columns">
                    <div class="column">
                    </div>
                    <div class="container column box">
                        <form action="Registro" method="post">
                            <div class="field">
                                <h1 class="title has-text-primary">Crear cuenta</h1>
                                <div style="display:none<c:out value="${existe}"/>;" class="notification is-warning">
                                    <p>Ese nombre de usuario ya está en uso</p>
                                </div>

                                <p class="control has-icons-left">
                                    <input class="input" type="text" placeholder="usuario" name="nombre" required="">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-user"></i>
                                    </span>
                                </p>
                            </div>
                            <div class="field">
                                <p class="control has-icons-left">
                                    <input
                                        class="input"
                                        type="password"
                                        placeholder="contraseña"
                                        name="contra"
                                        required=""
                                        pattern="^(?=.*\d).{4,8}$"
                                        title="La contraseña debe estar entre 4 a 8 caracteres y debe contener al menos un número">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-key"></i>
                                    </span>
                                </p>
                            </div>
                            <div class="field center">
                                <p class="control buttons">
                                    <button class="button is-success is-rounded" name="botonEnviar">
                                        Regístrate!
                                    </button>
                                </p>
                            </div>
                        </form>
                    </div>
                    <div class="column">
                    </div>
                </div>
            </div>
            <div class="hero-foot">
                <p class="center">
                    <strong>Agenda Telefónica</strong>&nbsp; by &nbsp;<strong>!FlippedBit</strong>
                </p>
            </div>
        </section>
    </body>
</html>
