<%--
    Document   : home
    Created on : Mar 19, 2019, 2:31:36 PM
    Author     : !FlippedBit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda</title>
        <link rel="stylesheet" href="css/bulma.css"/>
        <link rel="stylesheet" href="css/all.css">
    </head>
    <style media="screen">
        .is-dark{
            background-image:url("images/pattern.jpeg");
            background-blend-mode:multiply;
            background-size:contain;
        }
    </style>
    <body>
        <%-- Hero de encabezado --%>
        <section class="hero is-dark">
            <div class="hero-body">
                <div class="container">
                    <h1 class="title is-1">
                        ¡Bienvenido <strong class="has-text-link"><c:out value="${userName}"/></strong>!
                    </h1>
                    <h2 class="subtitle">
                        Estos son sus contactos
                    </h2>
                </div>
                <div class="level">
                    <div class="level-left">
                    </div>
                    <div class="level-right">
                        <div class="level-item buttons" style="margin-right:115px;">
                            <p class="control">
                            <form action="newContact.jsp" method="post">
                                <button class="button is-success is-rounded" name="button">
                                    <p>Agregar</p>
                                    <span class="icon">
                                        <i class="fas fa-plus"></i>
                                    </span>
                                </button>
                            </form>
                            </p>
                            <p class="control" style="margin-left:5px;">
                            <form action="LogOut" method="post">
                                <button class="button is-danger is-rounded" name="button">
                                    <p>Cerrar Sesión</p>
                                    <span class="icon">
                                        <i class="fas fa-sign-out-alt"></i>
                                    </span>
                                </button>
                            </form>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="columns">
            <div class="column">
            </div>
            <section class="section column is-three-fifths">

                <c:forEach items="${lista}" var="uno">
                    <div class="box ">
                        <article class="media columns">
                            <div class="media-left">
                                <figure class="image is-64x64">
                                    <img src="images/profile.png" alt="ProfilePic">
                                </figure>
                            </div>
                            <div class="media-content column">
                                <p>
                                    <strong class="title is-4">${uno.contacto}</strong>
                                    <br>
                                </p>
                                <p>
                                    <b>Numero: </b>
                                    ${uno.numero}
                                    <b>Tipo: </b>
                                    ${uno.tipoDeNumero}
                                </p>
                            </div>
                            <div class="container column">
                                <form action="RequestUpdate" method="post">
                                    <p class="control is-pulled-right">
                                        <button class="button is-warning is-rounded is-small" style="margin-bottom:8px;" name="edit" value="${uno.idRel}">
                                            <p>
                                                &nbsp;
                                                Editar
                                                &nbsp;
                                            </p>
                                            <span class="icon is-small">
                                                <i class="fas fa-user-edit"></i>
                                            </span>
                                        </button>
                                    </p>
                                </form>
                                <p class="control is-pulled-right">
                                    <button class="button is-danger is-rounded is-small elimina" id="delete" name="delete" value="${uno.idRel}">
                                        <p>
                                            Eliminar
                                        </p>
                                        <span class="icon is-small">
                                            <i class="fas fa-times"></i>
                                        </span>
                                    </button>
                                </p>
                            </div>
                        </article>
                    </div>
                </c:forEach>

            </section>
            <div class="column">
            </div>
        </div>
        <div class="modal">
            <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head">
                    <p class="modal-card-title">Eliminar</p>
                </header>
                <section class="modal-card-body">
                    <p>¿Está seguro de que desea eliminar el contacto?</p>
                </section>
                <footer class="modal-card-foot">
                    <form action="EliminaContacto" method="post">
                        <input name="idRel" class="rel" type="hidden">
                        <button class="button is-danger final">Eliminar</button>
                    </form>
                    <button class="button is-success close" style="margin-left:8px;" id="modal-close">Cancelar</button>
                </footer>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript">
        var modal = $(".modal");
        var botonDelete = $(".elimina");
        var rel = $(".rel");
        var botonFinal = $(".final");

        botonDelete.click(function () {
            var id = $(this).val();
            modal.addClass("is-active");
            rel.val(id);
        });

        $(".close").click(function () {
            $(".modal").removeClass("is-active");
        });
    </script>
</html>
