<%--
Document   : index
Created on : Mar 11, 2019, 11:18:56 PM
Author     : flippedbit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inicia sesión</title>
        <link rel="stylesheet" href="css/bulma.css">
        <link rel="stylesheet" href="css/all.css">
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <style media="screen">
        .is-link{
            background-image: url("images/loginalt.jpg");
            background-blend-mode: multiply;
        }
    </style>
    <body>
        <section class="hero is-link is-fullheight">
            <div class="hero-head">
                <div class="container">
                    <h1 class="title is-1">Agenda Telefónica</h1>
                </div>
            </div>
            <div class="hero-body center">
                <div class="columns">
                    <div class="column">
                    </div>
                    <div class="container column box">
                        <form action="Autenticacion" method="post">
                            <div class="field">
                                <h1 class="title has-text-link">Iniciar Sesión</h1>
                                <div style="display:none<c:out value="${error}"/>;" class="notification is-danger">
                                    <p>El usuario no existe, ¡Regístrate!</p>
                                </div>
                                <div style="display:none<c:out value="${warn}"/>;" class="notification is-warning">
                                    <p>Nombre de usuario o contraseña incorrectos</p>
                                </div>

                                <p class="control has-icons-left">
                                    <input class="input" type="text" placeholder="usuario" name="nombre" required="">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-user"></i>
                                    </span>
                                </p>
                            </div>
                            <div class="field">
                                <p class="control has-icons-left">
                                    <input class="input" type="password" placeholder="contraseña" name="contra" required="">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-key"></i>
                                    </span>
                                </p>
                            </div>
                            <div class="field center">
                                <p class="control buttons">
                                    <button class="button is-success is-rounded" name="botonEnviar">
                                        Ingresar
                                    </button>
                                    <a class="button is-warning is-rounded" href="registro.jsp" type="button" name="botonReg">
                                        Regístrate
                                    </a>
                                </p>
                            </div>
                        </form>
                    </div>
                    <div class="column">
                    </div>
                </div>
            </div>
            <div class="hero-foot">
                <p class="center">
                    <strong>Agenda Telefónica</strong>&nbsp; by &nbsp;<strong>!FlippedBit</strong>
                </p>
            </div>
        </section>
    </body>
</html>
