    <%--
        Document   : newContact
        Created on : Mar 20, 2019, 1:48:50 PM
        Author     : !FlippedBit
    --%>

    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Nuevo Contacto</title>
            <link rel="stylesheet" href="css/bulma.css"/>
            <link rel="stylesheet" href="css/all.css">
        </head>
        <style media="screen">
            .is-dark{
                background-image:url("images/plus.png");
                background-blend-mode:multiply;
            }
        </style>
        <body>
            <section class="hero is-dark">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title is-1">
                            Añade un <b class="has-text-success"> nuevo </b> contacto
                        </h1>
                        <h2 class="subtitle">
                            Agrega un nuevo contacto a tu lista
                        </h2>
                    </div>
                    <div class="level">
                        <div class="level-left">
                        </div>
                        <div class="level-right">
                            <div class="level-item buttons" style="margin-right:115px;">
                                <p class="control">
                                    <form action="home.jsp" method="post">
                                    <button class="button is-warning is-rounded" name="button">
                                        <p>Volver</p>
                                        <span class="icon">
                                            <i class="fas fa-chevron-left"></i>
                                        </span>
                                    </button>
                                </form>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="columns">
                <div class="column">
                </div>
                <section class="section column is-three-fifths">
                    <div class="has-text-center box">
                        <h1 class="title">Datos de Contacto</h1>
                        <form action="NewContact" method="post">
                        <div class="field is-horizontal">
                          <div class="field-label is-normal">
                            <label class="label">Nombre</label>
                          </div>
                          <div class="field-body">
                            <div class="field">
                                <p class="control has-icons-left">
                                  <input name="nombreContacto" class="input" type="text" placeholder="Nombre de Contacto">
                                  <span class="icon is-small is-left">
                                    <i class="fas fa-user"></i>
                                  </span>
                                </p>
                            </div>
                          </div>
                        </div>

                        <div class="field is-horizontal">
                          <div class="field-label is-normal">
                            <label class="label">Número</label>
                          </div>
                          <div class="field-body">
                            <div class="field">
                                <p class="control has-icons-left">
                                  <input
                                      name="NumeroContacto"
                                      class="input"
                                      type="text"
                                      placeholder="Número de Contacto"
                                      pattern="^[1-9]+[0-9]*$"
                                      title="Este campo solo puede contener números">
                                  <span class="icon is-small is-left">
                                    <i class="fas fa-phone"></i>
                                  </span>
                                </p>
                            </div>
                          </div>
                        </div>


                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">Tipo de Telefono</label>
                            </div>
                            <div class="field-body">
                                <div class="field is-narrow">
                                    <p class="control has-icons-left">
                                        <span class="select">
                                            <select name="tipoNum">
                                                <option selected>-----------</option>
                                                <c:forEach items="${tipos}" var="uno">
                                                    <option value="${uno.idTipo}">${uno.nombreTipo}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-list-ul"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                          <div class="field-label">
                          </div>
                          <div class="field-body">
                            <div class="field">
                              <div class="control">
                                <button class="button is-success">
                                  <span>
                                      <i class="fas fa-plus"></i>
                                  </span>
                                  <p>&nbsp; Agregar</p>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </form>
                    </div>
                </section>
                <div class="column">
                </div>
            </div>
        </body>
    </html>
